module Config
  Name = 'Snake'
  ScreenWidth, ScreenHeight = 640, 480
  TableWidth, TableHeight = 20, 20
  StandardSpeed = 5
  StandardMap = 'box'
  Rootpath = File.realpath(File.dirname(__FILE__)+'/..')
end

module Version
  MAJOR = 0
  MINOR = 0
  TINY  = 2
  PRE = nil
  STRING  = [MAJOR, MINOR, TINY, PRE].compact.join(".")
end
