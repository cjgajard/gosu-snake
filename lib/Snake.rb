class Snake
  attr_accessor :score, :dots
  def initialize
    @dir = :right
    @dots = [ Coordenate.new(9,10), Coordenate.new(10,10) ]
    @score = 0
    @dir_candidate = nil
  end

  def switch(dir)
    if \
      dir ==    :up && @dir !=  :down || \
      dir == :right && @dir !=  :left || \
      dir ==  :down && @dir !=    :up || \
      dir ==  :left && @dir != :right
      @dir_candidate = dir
    end
  end

  def turn
    if @dir_candidate
      @dir = @dir_candidate 
      @dir_candidate = nil
    end
  end

  def move(width, height)
    x, y = @dots[-1].x, @dots[-1].y 
    case @dir
    when :right then @dots << Coordenate.new(x+1,  y )
    when    :up then @dots << Coordenate.new( x , y-1)
    when  :left then @dots << Coordenate.new(x-1,  y )
    when  :down then @dots << Coordenate.new( x , y+1)
    end
    @dots[-1].x %= width
    @dots[-1].y %= height
  end

  def eat?(food)
    @dots[-1] == food.position
  end

  def collide?(walls)
    return true if @dots.any? {|dot| walls.bricks.include?(dot)}
    @dots.uniq.size < @dots.size ? true : false
  end
end
