class GameWindow < Gosu::Window
  def initialize
    super Config::ScreenWidth, Config::ScreenHeight
    self.caption = Config::Name

    @width, @height = Config::TableWidth, Config::TableHeight
    @speed = ARGV[0].nil? ? Config::StandardSpeed : ARGV[0].to_i
    # @level = ARGV[1] || Config::StandardMap

    @walls = Wall.new #(@level)
    @snake = Snake.new
    @food = Food.new; @food.reset(@width, @height, @walls.bricks, @snake.dots)

    @font = Gosu::Font.new(20)
  end

  private
  def update
    @frames = (0.06*Gosu.milliseconds).to_i
    if Gosu::button_down? Gosu::KbLeft or Gosu::button_down? Gosu::GpLeft
      @snake.switch :left
    elsif Gosu::button_down? Gosu::KbRight or Gosu::button_down? Gosu::GpRight
      @snake.switch :right
    elsif Gosu::button_down? Gosu::KbDown or Gosu::button_down? Gosu::GpDown
      @snake.switch :down
    elsif Gosu::button_down? Gosu::KbUp or Gosu::button_down? Gosu::GpUp
      @snake.switch :up
    end
    
    if @frames % (60/@speed) == 0
      @snake.turn
      if @snake.eat?(@food)
        @snake.score += @speed
        @food.reset(@width, @height, @walls.bricks, @snake.dots)
      else
        @snake.dots.delete_at(0)
      end
      @snake.move(@width, @height)
      close if @snake.collide?(@walls)
    end
  end

  def draw
    # @background_image.draw(99, 19, 0)

    draw_pixel(@food.position.x, @food.position.y, 0xff_ffff00)
    @snake.dots.each do |dot|
      draw_pixel(dot.x, dot.y)
    end
    @walls.bricks.each do |brick|
      draw_pixel(brick.x, brick.y, 0xff_202020)
    end
    
    @font.draw("Speed: #{@speed}", 10, 10, 3, 1.0, 1.0, 0xff_ffffff)
    @font.draw("Score: #{@snake.score}", 10, 30, 3, 1.0, 1.0, 0xff_ffffff)
    # @font.draw("HighScore: #{@snake.score}", 10, 50, 3, 1.0, 1.0, 0xff_ffffff)
  end

  def close
    super
    puts 'Game Over', "Score: #{@snake.score}"
    # if @snake.score > @highscore
  end

  def button_down(id)
    close if id == Gosu::KbEscape
  end

  def draw_pixel(x,y,color=0xff_ffffff)
    Gosu.draw_rect(22*x+161, 22*y+21, 20, 20, color)
  end
end
