class Food
  attr_reader :position
  def initialize
    @position = Coordenate.new
  end

  def reset(width, height, bricks, dots)
    availible = []
    width.times do |i|
      # next if bricks.none? {|b| b.x==i} || dots.none? {|d| d.x==i}
      height.times do |j|
        # next if bricks.none? {|b| b.y==i} || dots.none? {|d| d.y==i}
        availible << [i,j] unless (bricks+dots).include?(Coordenate.new(i,j))
      end
    end
    @position.x, @position.y = *availible.sample
  end
end
