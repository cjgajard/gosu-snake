require 'gosu'
require_relative 'Config'
require_relative 'GameWindow'

Coordenate = Struct.new(:x, :y)
require_relative 'Wall'
require_relative 'Snake'
require_relative 'Food'
