class Wall
  attr_reader :bricks
  def initialize(level=1)
    # add level reader
    @bricks = \
      20.times.map {|i| Coordenate.new( i,   0)} + \
      20.times.map {|i| Coordenate.new( i,  19)} + \
      18.times.map {|i| Coordenate.new( 0, i+1)} + \
      18.times.map {|i| Coordenate.new(19, i+1)}
  end
end
