# Gosu-snake

## Installation

## Usage
```bash
ruby snake
```
You can set a different speed (default = 5)  
```bash
ruby snake 10
```

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Contributors
- [cjgajard](https://github.com/cjgajard) Carlos Gajardo - creator, maintainer

## License
MIT
